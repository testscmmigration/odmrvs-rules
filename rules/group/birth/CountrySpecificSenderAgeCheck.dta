<?xml version="1.0" encoding="UTF-8"?>
<ilog.rules.studio.model.dt:DecisionTable xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:ilog.rules.studio.model.dt="http://ilog.rules.studio/model/dt.ecore">
  <eAnnotations source="ilog.rules.custom_properties">
    <details key="API" value="(SEND,BP)"/>
    <details key="Business Group" value="Compliance Validation Rules"/>
    <details key="Business User" value="Cheryl Magers"/>
    <details key="Prod Date" value="11/30/2017"/>
    <details key="Rule ID" value="DQ135"/>
  </eAnnotations>
  <name>CountrySpecificSenderAgeCheck</name>
  <uuid>c048db9d-eed5-4f0e-809b-d690d19529f3</uuid>
  <locale>en_US</locale>
  <definition>
<DT xmlns="http://schemas.ilog.com/Rules/7.0/DecisionTable" Version="7.0">
  <Body>
    <Preconditions>
      <Text><![CDATA[definitions]]>&#13;<![CDATA[
    set 'the birth' to a business entity in the entities of 'the validation request']]>&#13;<![CDATA[
            where the name of this business entity is "birthInfo" ;]]>&#13;<![CDATA[
    set 'the date of birth' to an attribute in the attributes of 'the birth']]>&#13;<![CDATA[
            where the name of this attribute is "dateOfBirth" ;]]>&#13;<![CDATA[
            ]]>&#13;<![CDATA[
if]]>&#13;<![CDATA[
        the value of 'the date of birth' is not null and]]>&#13;<![CDATA[
        the value of 'the date of birth' is NOT EMPTY and]]>&#13;<![CDATA[
		'the valid date format' is true]]></Text>
    </Preconditions>
    <Structure>
      <ConditionDefinitions>
        <ConditionDefinition Id="C0">
          <ExpressionDefinition>
            <Text><![CDATA[the country code of 'the agent ISOcountry' is one of <objects>]]></Text>
          </ExpressionDefinition>
        </ConditionDefinition>
        <ConditionDefinition Id="C1">
          <ExpressionDefinition>
            <Text><![CDATA[the customer age with DOB the value of 'the date of birth' is <an object>]]></Text>
          </ExpressionDefinition>
        </ConditionDefinition>
        <ConditionDefinition Id="C2">
          <ExpressionDefinition>
            <Text><![CDATA['is option O' is <a boolean>]]></Text>
          </ExpressionDefinition>
        </ConditionDefinition>
      </ConditionDefinitions>
      <ActionDefinitions>
        <ActionDefinition Id="A0">
          <ExpressionDefinition>
            <Text><![CDATA[set validation error for the entity name of 'the date of birth'   with rule id <a string> , rule name <a string> , rule reference id <a string> , error code <a string> , error message <a string> , persistence flag <a boolean> and verifyField flag <a boolean> to 'the validation response']]></Text>
          </ExpressionDefinition>
        </ActionDefinition>
      </ActionDefinitions>
    </Structure>
    <Contents>
      <Partition DefId="C0">
        <Condition>
          <Expression>
            <Param><![CDATA[{ AMERICAN SAMOA,ANGUILLA,ANTIGUA AND BARBUDA,ARGENTINA,AUSTRALIA,AUSTRIA,BAHAMAS,BELGIUM,BHUTAN,BOLIVIA,BOSNIA AND HERZEGOVINA,BOTSWANA,BRAZIL,CANADA,CAPE VERDE,CHILE,COLOMBIA,COSTA RICA,CYPRUS,CZECH REPUBLIC,DENMARK,DOMINICAN REPUBLIC,ECUADOR,EL SALVADOR,ERITREA,ESTONIA,ETHIOPIA,FIJI,FINLAND,GAMBIA,GERMANY,GHANA,GIBRALTAR,GUAM,GUATEMALA,HONDURAS,HONG KONG,HUNGARY,ICELAND,INDIA,IRELAND,JAMAICA,KOSOVO,KYRGYZSTAN,LATVIA,LESOTHO,LIECHTENSTEIN,LITHUANIA,LUXEMBOURG,MACAO,MACEDONIA FORMER YUGOSLAV REPUBLIC,MALAWI,MALDIVES,MALTA,MARSHALL ISLANDS,MAURITANIA,MICRONESIA FEDERATED STATES OF,MOZAMBIQUE,NAMIBIA,NEW ZEALAND,NORWAY,PANAMA,PAPUA NEW GUINEA,PARAGUAY,PERU,PORTUGAL,SAINT LUCIA,SAINT VINCENT AND THE GRENADINES,SERBIA,SLOVAKIA,SLOVENIA,SOLOMON ISLANDS,SOUTH SUDAN,SPAIN,SWAZILAND,TAJIKISTAN,TANZANIA UNITED REPUBLIC OF,TIMOR LESTE,TONGA,TUVALU,UGANDA,UNITED KINGDOM,UNITED STATES,URUGUAY,VANUATU,ZAMBIA,AFGHANISTAN,EGYPT,IRAQ,LIBYA,PALESTINIAN TERRITORY OCCUPIED,ARUBA,BARBADOS,BERMUDA,CAYMAN ISLANDS,GRENADA,GUYANA,HAITI,MONTSERRAT,SINT MAARTEN DUTCH SIDE,SURINAME,LIBERIA,MAURITIUS,NIGERIA,SIERRA LEONE,ZIMBABWE,BAHRAIN,BANGLADESH,CHINA,JORDAN,KOREA REPUBLIC OF,KUWAIT,LEBANON,OMAN,PAKISTAN,QATAR,SRI LANKA,TAIWAN,UNITED ARAB EMIRATES,YEMEN,ALBANIA,ARMENIA,BELARUS,BULGARIA,CROATIA,FRANCE,GEORGIA,GREECE,ITALY,KAZAKHSTAN,MOLDOVA,NETHERLANDS,POLAND,ROMANIA,RUSSIAN FEDERATION,SAINT KITTS AND NEVIS,SWEDEN,SWITZERLAND,TRINIDAD AND TOBAGO,TURKEY,TURKS AND CAICOS ISLANDS,UKRAINE,UZBEKISTAN,VENEZUELA,JAPAN,FRENCH GUIANA,GUADELOUPE,MARTINIQUE,MAYOTTE,REUNION,TURKMENISTAN,GUERNSEY,PUERTO RICO,VIRGIN ISLANDS BRITISH }]]></Param>
          </Expression>
          <Partition DefId="C1">
            <Condition>
              <Expression>
                <Text><![CDATA[<a number> is less than <a number>]]></Text>
                <Param><![CDATA[18]]></Param>
              </Expression>
              <Partition DefId="C2">
                <Condition>
                  <Expression>
                    <Param><![CDATA[false]]></Param>
                  </Expression>
                  <ActionSet>
                    <Action DefId="A0">
                      <Expression>
                        <Param><![CDATA["DQ135"]]></Param>
                        <Param><![CDATA["Country Specific Sender Age Check"]]></Param>
                        <Param><![CDATA["R4212"]]></Param>
                        <Param><![CDATA["4212"]]></Param>
                        <Param><![CDATA["Customer must be at least 18 years old to send money."]]></Param>
                        <Param><![CDATA[false]]></Param>
                        <Param><![CDATA[false]]></Param>
                      </Expression>
                    </Action>
                  </ActionSet>
                </Condition>
              </Partition>
            </Condition>
          </Partition>
        </Condition>
        <Condition>
          <Expression>
            <Param><![CDATA[{ INDONESIA }]]></Param>
          </Expression>
          <Partition DefId="C1">
            <Condition>
              <Expression>
                <Text><![CDATA[<a number> is less than <a number>]]></Text>
                <Param><![CDATA[17]]></Param>
              </Expression>
              <Partition DefId="C2">
                <Condition>
                  <Expression>
                    <Param><![CDATA[false]]></Param>
                  </Expression>
                  <ActionSet>
                    <Action DefId="A0">
                      <Expression>
                        <Param><![CDATA["DQ135"]]></Param>
                        <Param><![CDATA["Country Specific Sender Age Check"]]></Param>
                        <Param><![CDATA["R4210"]]></Param>
                        <Param><![CDATA["4210"]]></Param>
                        <Param><![CDATA["Customer must be at least 17 years old to send money."]]></Param>
                        <Param><![CDATA[false]]></Param>
                        <Param><![CDATA[false]]></Param>
                      </Expression>
                    </Action>
                  </ActionSet>
                </Condition>
              </Partition>
            </Condition>
          </Partition>
        </Condition>
        <Condition>
          <Expression>
            <Param><![CDATA[{ ALGERIA,BELIZE,BRAZIL,BRUNEI DARUSSALAM,BURUNDI,CAMBODIA,CURACAO,CENTRAL AFRICAN REPUBLIC,CHAD,COMOROS,DJIBOUTI,EQUATORIAL GUINEA,GABON,GUINEA,GUINEA BISSAU,LAO PEOPLE S DEMOCRATIC REPUBLIC,MADAGASCAR,MEXICO,MYANMAR,NICARAGUA,PARAGUAY,RWANDA,SAMOA,SAO TOME AND PRINCIPE,SEYCHELLES,TOGO,TUNISIA,VIETNAM,DOMINICA,VIRGIN ISLANDS US,BENIN,BURKINA FASO,CAMEROON,CONGO,CONGO DEMOCRATIC REPUBLIC OF THE,COTE DIVOIRE,MALI,MOROCCO,NIGER,SENEGAL,MALAYSIA,PHILIPPINES,MONGOLIA,SINGAPORE }]]></Param>
          </Expression>
          <Partition DefId="C1">
            <Condition>
              <Expression>
                <Text><![CDATA[<a number> is less than <a number>]]></Text>
                <Param><![CDATA[16]]></Param>
              </Expression>
              <Partition DefId="C2">
                <Condition>
                  <Expression>
                    <Param><![CDATA[false]]></Param>
                  </Expression>
                  <ActionSet>
                    <Action DefId="A0">
                      <Expression>
                        <Param><![CDATA["DQ135"]]></Param>
                        <Param><![CDATA["Country Specific Sender Age Check"]]></Param>
                        <Param><![CDATA["R4208"]]></Param>
                        <Param><![CDATA["4208"]]></Param>
                        <Param><![CDATA["Customer must be at least 16 years old to send money."]]></Param>
                        <Param><![CDATA[false]]></Param>
                        <Param><![CDATA[false]]></Param>
                      </Expression>
                    </Action>
                  </ActionSet>
                </Condition>
              </Partition>
            </Condition>
          </Partition>
        </Condition>
        <Condition>
          <Expression>
            <Param><![CDATA[{ ANGOLA }]]></Param>
          </Expression>
          <Partition DefId="C1">
            <Condition>
              <Expression>
                <Text><![CDATA[<a number> is less than <a number>]]></Text>
                <Param><![CDATA[14]]></Param>
              </Expression>
              <Partition DefId="C2">
                <Condition>
                  <Expression>
                    <Param><![CDATA[false]]></Param>
                  </Expression>
                  <ActionSet>
                    <Action DefId="A0">
                      <Expression>
                        <Param><![CDATA["DQ135"]]></Param>
                        <Param><![CDATA["Country Specific Sender Age Check"]]></Param>
                        <Param><![CDATA["R4221"]]></Param>
                        <Param><![CDATA["4221"]]></Param>
                        <Param><![CDATA["Customer must be at least 14 years of age to send money."]]></Param>
                        <Param><![CDATA[false]]></Param>
                        <Param><![CDATA[false]]></Param>
                      </Expression>
                    </Action>
                  </ActionSet>
                </Condition>
              </Partition>
            </Condition>
          </Partition>
        </Condition>
      </Partition>
    </Contents>
  </Body>
  <Resources DefaultLocale="en_US">
    <ResourceSet Locale="en_US">
      <Data Name="Definitions(A0)#HeaderText"><![CDATA[Set Validation Error]]></Data>
      <Data Name="Definitions(A0)#Width"><![CDATA[462]]></Data>
      <Data Name="Definitions(A0)[0]#HeaderText"><![CDATA[Rule ID]]></Data>
      <Data Name="Definitions(A0)[1]#HeaderText"><![CDATA[Rule Name]]></Data>
      <Data Name="Definitions(A0)[2]#HeaderText"><![CDATA[Rule Ref ID]]></Data>
      <Data Name="Definitions(A0)[3]#HeaderText"><![CDATA[Error Code]]></Data>
      <Data Name="Definitions(A0)[4]#HeaderText"><![CDATA[Error Message]]></Data>
      <Data Name="Definitions(A0)[5]#HeaderText"><![CDATA[Persitence Flag]]></Data>
      <Data Name="Definitions(A0)[6]#HeaderText"><![CDATA[VerifyField Flag]]></Data>
      <Data Name="Definitions(C0)#HeaderText"><![CDATA[Agent ISO Country]]></Data>
      <Data Name="Definitions(C0)#Width"><![CDATA[456]]></Data>
      <Data Name="Definitions(C1)#HeaderText"><![CDATA[Customer Age]]></Data>
      <Data Name="Definitions(C1)#Width"><![CDATA[444]]></Data>
      <Data Name="Definitions(C2)#HeaderText"><![CDATA[Is Option O]]></Data>
      <Data Name="Definitions(C2)#Width"><![CDATA[443]]></Data>
    </ResourceSet>
  </Resources>
</DT></definition>
  <effectiveDate>2017-09-01T00:00:00.000-0500</effectiveDate>
  <expirationDate>2099-12-01T00:00:00.000-0600</expirationDate>
</ilog.rules.studio.model.dt:DecisionTable>
